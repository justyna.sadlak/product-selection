import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { products } from './products-list';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'product-selection';
  products = products;
  product = new FormControl();

  ngOnInit() {
    this.product.valueChanges.subscribe(val => console.log(val));
  }
}
