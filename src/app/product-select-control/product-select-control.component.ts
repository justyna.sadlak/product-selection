import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Product } from './product.model';

@Component({
  selector: 'app-product-select-control',
  templateUrl: './product-select-control.component.html',
  styleUrls: ['./product-select-control.component.scss'],
  providers: [
      {
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => ProductSelectControlComponent),
          multi: true
      }
  ]
})
export class ProductSelectControlComponent implements ControlValueAccessor {
  @Input() products: Product[];
  selectedValue: Product;
  
  selectOption(option: Product) {
    this.selectedValue = option;
    this.writeValue(option);
  }
  
  writeValue(value: Product): void {
    this.selectedValue = value;
    this.onChange(this.selectedValue);
  }

  onChange = (_:Product) => { };
  onTouched: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
