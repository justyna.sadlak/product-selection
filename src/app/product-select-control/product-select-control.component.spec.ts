import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ProductSelectControlComponent } from './product-select-control.component';

describe('ProductSelectControlComponent', () => {
  let component: ProductSelectControlComponent;
  let fixture: ComponentFixture<ProductSelectControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductSelectControlComponent ],
      imports: [
        NgbModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSelectControlComponent);
    component = fixture.componentInstance;
    fixture.debugElement.injector.get(NG_VALUE_ACCESSOR);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should match with products', () => {
    component.products = [
      {
        id: 1,
        name: 'test',
        description: 'test'
      },
      {
        id: 2,
        name: 'test2',
        description: 'test2'
      }
    ];
    fixture.detectChanges();
    // fixture.debugElement.query(By.css('.product-block')).nativeElement.click();
    const blockElement = fixture.nativeElement;
    const productBlock = blockElement.querySelector('.product-block');
    productBlock.click();
    expect(component.selectedValue).toEqual({
      id: 1,
      name: 'test',
      description: 'test'
    });
  })
});
