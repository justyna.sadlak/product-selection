import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { ProductSelectControlComponent } from './product-select-control/product-select-control.component';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ProductSelectControlComponent
      ],
      imports: [
        NgbModule
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
  });

  it('should create the app', () => {
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'product-selection'`, () => {
    const app = fixture.componentInstance;
    expect(app.title).toEqual('product-selection');
  });

  it('should have <button> with "Wybór Produktów"', () => {
    const blockElement: HTMLElement = fixture.nativeElement;
    const button = blockElement.querySelector('button');
    expect(button?.textContent?.trim()).toEqual('Wybór Produktów');
  });
});
