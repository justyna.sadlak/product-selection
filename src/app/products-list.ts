export const products = [
    {
        id: 1,
        name: 'Produkt 1',
        description: 'Produkt pierwszy'
    },
    {
        id: 2,
        name: 'Produkt 2',
        description: 'Produkt drugi'
    },
    {
        id: 3,
        name: 'Produkt 3',
        description: 'Produkt trzeci'
    },
    {
        id: 4,
        name: 'Produkt 4',
        description: 'Produkt czwarty'
    },
    {
        id: 5,
        name: 'Produkt 5',
        description: 'Produkt piąty'
    },
    {
        id: 6,
        name: 'Produkt 6',
        description: 'Produkt szósty'
    },
]